package com.sda.string;

import java.util.Scanner;

public class Zadanie10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj tekst: ");
        String tekst = scanner.nextLine();
        String[] tablica = tekst.split(" ");
        for (String wynik : tablica) {
            System.out.println(wynik);
        }
    }
}


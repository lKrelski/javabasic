package com.sda.string;

import java.util.Scanner;

public class Zadanie7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj tekst: ");
        String tekst = scanner.nextLine();
        String tekst2 = tekst.replace('ł', 'l').replace('Ł', 'L').replace('Ą', 'A')
                .replace('ą', 'a').replace('ę', 'e').replace('Ę', 'E').replace('ć', 'c')
                .replace('Ć', 'C').replace('Ó', 'O').replace('ó', 'o').replace('Ź', 'Z')
                .replace('Ż', 'Z').replace('ź', 'z').replace('ż', 'z').replace('Ń', 'N')
                .replace('Ś', 'S').replace('ń', 'n').replace('ś', 's');
        System.out.println(tekst2);
    }
}

package com.sda.string;

import java.util.Scanner;

public class Zadanie9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj pierwszy tekst: ");
        String tekst1 = scanner.nextLine();
        System.out.print("Podaj drugi tekst: ");
        String tekst2 = scanner.nextLine();
        String tekst3 = tekst1.toLowerCase();
        String tekst4 = tekst2.toLowerCase();
        if (tekst3.equals(tekst4)) {
            System.out.println("Wprowadzone teksty są takie same.");
        } else {
            System.out.println("Wprowadzone teksty są różne.");
        }
    }
}

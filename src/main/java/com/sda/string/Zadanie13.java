package com.sda.string;

import java.util.Scanner;

public class Zadanie13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj imię: ");
        String imie = scanner.nextLine();
        String msg = "Cześć %s, jestem Java i miło mi się z Tobą pracuje";
        System.out.println(String.format(msg,imie));
    }
}

package com.sda.string;

import java.util.Scanner;

public class Zadanie14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj tekst: ");
        String tekst = scanner.nextLine();
        System.out.print("Podaj liczbę znaków: ");
        int liczbaZnakow = scanner.nextInt();
        String tekst1 = tekst.substring(0,liczbaZnakow);
        System.out.println(tekst1);
    }
}

package com.sda.string;

import java.util.Scanner;

public class Zadanie8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj pierwszy tekst: ");
        String tekst1 = scanner.nextLine();
        System.out.print("Podaj drugi tekst: ");
        String tekst2 = scanner.nextLine();
        if (tekst1.equals(tekst2)) {
            System.out.println("Wprowadzone teksty są takie same.");
        } else {
            System.out.println("Wprowadzone teksty są różne.");
        }
    }
}

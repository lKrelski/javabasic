package com.sda.string;

import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj tekst: ");
        String tekst = scanner.nextLine();
        System.out.println("Pierwszy znak: "+ tekst.charAt(0)+ " ostatni znak: "+tekst.charAt(tekst.length()-1));
    }
}

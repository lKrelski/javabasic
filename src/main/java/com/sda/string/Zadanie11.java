package com.sda.string;

import java.util.Scanner;

public class Zadanie11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj pierwszy tekst: ");
        String tekst1 = scanner.nextLine();
        System.out.print("Podaj drugi tekst: ");
        String tekst2 = scanner.nextLine();
        String tekst3 = tekst1.concat(tekst2);
        System.out.println(tekst3);
    }
}

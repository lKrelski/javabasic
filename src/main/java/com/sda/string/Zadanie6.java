package com.sda.string;

import java.util.Scanner;

public class Zadanie6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj tekst: ");
        String tekst = scanner.nextLine();
        String tekstBezSpacji = tekst.replaceAll("\\s+", "");
        System.out.println("Tekst bez spacji: " + tekstBezSpacji);
    }
}

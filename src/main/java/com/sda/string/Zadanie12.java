package com.sda.string;

import java.util.Scanner;

public class Zadanie12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj tekst: ");
        String tekst = scanner.nextLine();
        String tekst1 = tekst.toLowerCase();
        if (tekst1.contains("programowanie")) {
            System.out.println("Tekst mi się podoba :)");
        } else {
            System.out.println("Wprowadzony ciąg znaków nie ma nic wspólnego z programowaniem :(");
        }
    }
}

package com.sda.oop.zadanie3;

public abstract class Car {
    private String producer;
    private String model;
    private String colour;
    private int seatsNumber;
    private Engine engine;

    public Car() {
        seatsNumber =2;
    }

    public Car(String producer, String model, String colour, int seatsNumber, Engine engine) {
        this.producer = producer;
        this.model = model;
        this.colour = colour;
        this.seatsNumber = seatsNumber;
        this.engine = engine;
    }
}

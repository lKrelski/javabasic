package com.sda.oop.zadanie3;

public class SportCar extends Car {

    public SportCar(String producer, String model, String colour, int seatsNumber, Engine engine) {
        super(producer, model, colour, seatsNumber, engine);
    }
}

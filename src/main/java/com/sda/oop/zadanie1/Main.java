package com.sda.oop.zadanie1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj pierwszą liczbę: ");
        int liczba1 = scanner.nextInt();
        System.out.print("Podaj drugą liczbę: ");
        int liczba2 = scanner.nextInt();
        System.out.print("Podaj operację (+,-,*,/): ");
        scanner.nextLine();
        String operacja = scanner.nextLine();
        Calculator calculator = new Calculator();
        switch ( operacja.charAt(0) ) {
            case '+':
                int wynikDodawania = calculator.sum(liczba1, liczba2);
                System.out.println("Wynik dodawania: " + wynikDodawania);
                break;
            case '-':
                int wynikOdejmowania = calculator.dif(liczba1, liczba2);
                System.out.println("Wynik odejmowania: " + wynikOdejmowania);
                break;
            case '/':
                float wynikDzielenia = calculator.quotient(liczba1, liczba2);
                System.out.println("Wynik dzielenia: " + wynikDzielenia);
                break;
            case '*':
                int wynikMnozenia = calculator.product(liczba1, liczba2);
                System.out.println("Wynik mnożenia: " + wynikMnozenia);
                break;
        }
    }
}

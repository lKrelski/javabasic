package com.sda.oop.zadanie1;

public class Calculator {
    public int sum(int value1, int value2) {
        int sum = value1 + value2;
        return sum;
    }

    public int dif(int value1, int value2) {
        int dif = value1 - value2;
        return dif;
    }

    public int product(int value1, int value2) {
        int product = value1 * value2;
        return product;
    }

    public float quotient(int value1, int value2) {
        float quotient = (float)value1 / value2;
        return quotient;
    }
}

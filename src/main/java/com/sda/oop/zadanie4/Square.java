package com.sda.oop.zadanie4;

public class Square extends Figure {
    int side;

    public Square(int side) {
        this.side = side;
    }
    @Override
    float countArea() {
        return side*side;
    }

    @Override
    void displayArea() {
        System.out.println("Figura: kwadrat, pole: "+countArea());
    }


}

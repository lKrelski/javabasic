package com.sda.oop.zadanie4;

public class Rectangle extends Figure {
    int sideA;
    int sideB;

    public Rectangle(int sideA, int sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }
    @Override
    float countArea() {
        return sideA * sideB;
    }

    @Override
    void displayArea() {
        System.out.println("Figura: prostokąt, " + "pole: " + countArea());
    }

}

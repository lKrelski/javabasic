package com.sda.oop.zadanie4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.print("Podaj liczbę prostokątów: ");
        int liczbaProstokatow = scanner.nextInt();
        System.out.print("Podaj liczbę kwadratów: ");
        int liczbaKwadratow = scanner.nextInt();
        System.out.print("Podaj liczbę kół: ");
        int liczbaKol = scanner.nextInt();
        List<Figure> figures = new ArrayList<>();
        for (int i = 0; i < liczbaProstokatow; i++) {
            figures.add(new Rectangle(random.nextInt(50), random.nextInt(50)));
        }
        for (int i = 0; i < liczbaKol; i++) {
            figures.add(new Circle(random.nextInt(50)));
        }
        for (int i = 0; i < liczbaKwadratow; i++) {
            figures.add(new Square(random.nextInt(50)));
        }
        for (Figure wynik : figures) {
            wynik.displayArea();
        }
    }
}

package com.sda.oop.zadanie4;

public class Circle extends Figure {
    int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    float countArea() {
        return (float) Math.PI * (radius * radius);
    }

    @Override
    void displayArea() {
        System.out.println("Figura: koło, pole: " + countArea());
    }


}

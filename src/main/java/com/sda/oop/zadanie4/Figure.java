package com.sda.oop.zadanie4;

public abstract class Figure {
    abstract float countArea();

    abstract void displayArea();

}

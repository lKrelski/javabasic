package com.sda.oop.zadanie5;


import java.util.ArrayList;
import java.util.List;

public class Veggie implements Pizza, Ingredients {
    private List<String> ingredients;
    private PizzaDough pizzaDough;

    public Veggie() {
    }

    public Veggie(List<String> ingredients, PizzaDough pizzaDough) {
        this.ingredients = ingredients;
        this.pizzaDough = pizzaDough;
    }

    @Override
    public void preparePizza() {
        System.out.println("******Przygotowanie pizzy Veggie*********");
        pizzaDough.preparePizzaDough();
        System.out.println("1. Pomidory wraz octem i startym ząbkiem czosnku zblenduj w małym rondlu.");
        System.out.println("2. Powstały sos gotuj aż do odparowania wody, aż osiągnie gęstą kosystencję (ok 15 min.).");
        System.out.println("3. Po zakończeniu gotowania dopraw ziołami i solą.");
        System.out.println("4. Nagrzej piekarnij do 220 stopnii.");
        System.out.println("5. Tempeh pokrój w kostkę, paprykę w paski, cukinię w ćwiartki.");
        System.out.println("6. W małym rondlu bądź patelni rozgrzej olej i podsmaż najpierw tempeh, później dodaj" +
                " cukinię i paprykę. Podczas smażenia możesz dodać odrobinę wody i sosu pomidorowego.");
        System.out.println("7. Na wyrośnięte ciasto pizzy nałóż łyżką sos pomidorowy, a następnie warzywa wraz z tempehem.");
        System.out.println("8. Posyp pizzę parmezanem i zapiekaj w piekarniku przez 10-15 minut do zarumienienia ciasta.");
    }

    @Override
    public List<String> getIngredients() {

        return ingredients;
    }

}

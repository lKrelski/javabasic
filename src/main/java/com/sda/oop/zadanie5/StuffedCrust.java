package com.sda.oop.zadanie5;

public class StuffedCrust implements PizzaDough {
    @Override
    public void preparePizzaDough() {
        System.out.println("Przygotowywanie ciasta StuffedCrust");
    }
}

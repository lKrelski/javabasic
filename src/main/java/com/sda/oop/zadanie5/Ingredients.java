package com.sda.oop.zadanie5;

import java.util.List;

public interface Ingredients {
    List<String> getIngredients();
    }


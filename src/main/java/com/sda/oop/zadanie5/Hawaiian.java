package com.sda.oop.zadanie5;


import java.util.ArrayList;
import java.util.List;

public class Hawaiian implements Pizza, Ingredients {
    private List<String> ingredients;
    private PizzaDough pizzaDough;

    public Hawaiian() {
    }

    public Hawaiian(List<String> ingredients, PizzaDough pizzaDough) {
        this.ingredients = ingredients;
        this.pizzaDough = pizzaDough;
    }

    @Override
    public void preparePizza() {
        System.out.println("**************Przygotowanie pizzy Hawaiian*************");
        pizzaDough.preparePizzaDough();
        System.out.println("1. Ciasto wyłożyć na największą blachę posmarowaną tłuszczem.");
        System.out.println("2. Posmarować sosem pomidorowym.");
        System.out.println("3. Posypać oregano i bazylią.");
        System.out.println("4. Nałożyć plastry szynki, plastry lub pokrojonego w kostkę ananasa i posypać startym serem.");
        System.out.println("5. Piec w nagrzanym piekarniku ok. 30minut w temperaturze 180°C.");
    }

    @Override
    public List<String> getIngredients() {

        return ingredients;
        }
    }


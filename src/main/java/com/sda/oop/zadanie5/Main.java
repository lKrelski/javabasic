package com.sda.oop.zadanie5;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> hawaiian = new ArrayList<>();
        List<String> veggie = new ArrayList<>();
        List<String> margherita = new ArrayList<>();
        hawaiian.add("ananas w puszce");
        hawaiian.add("200g szynki");
        hawaiian.add("sos pomidorowy (lub koncentrat pomidorowy, albo ketchup)");
        hawaiian.add("200g sera żółtego typu Ementaler");
        hawaiian.add("suszone oregano");
        hawaiian.add("suszona bazylia");
        veggie.add("pół kostki wędzonego tempehu (ok. 90g)");
        veggie.add("1 mała cukinia");
        veggie.add("po połówce papryki czerwonej i zielonej");
        veggie.add("1 łyżeczka oleju");
        veggie.add("parmezan z nerkowców do posypania");
        margherita.add("150ml sosu pomidorowego (pulpy pomidorowej)");
        margherita.add("250g sera mozzarella");
        margherita.add("12 listków świeżej bazylii");
        margherita.add("2 łyżki oliwy z oliwek");
        margherita.add("sól ziołowa");
        List<Pizza> pizzas = new ArrayList<>();
        pizzas.add(new Hawaiian(hawaiian, new FeelGoodFlatbread()));
        pizzas.add(new Veggie(veggie, new GlutenFree()));
        pizzas.add(new Margherita(margherita, new StuffedCrust()));
        for (Pizza wynik: pizzas) {
           wynik.preparePizza();
        }
    }
}

package com.sda.oop.zadanie5;


import java.util.ArrayList;
import java.util.List;

public class Margherita implements Pizza, Ingredients {
    private List<String> ingredients;
    private PizzaDough pizzaDough;
    public Margherita() {
    }

    public Margherita(List<String> ingredients, PizzaDough pizzaDough) {
        this.ingredients = ingredients;
        this.pizzaDough = pizzaDough;
    }

    @Override
    public void preparePizza() {
        System.out.println("***********Przygotowanie pizzy Margherita**********");
        pizzaDough.preparePizzaDough();
        System.out.println("1. Na dwóch blachach wyłożonych papierem do pieczenia uformować z ciasta placek w kształcie koła.");
        System.out.println("2. Pizze posmarować sosem pomidorowym. Przyprawić solą.");
        System.out.println("3. Mozzarellę osuszyć i pokroić w plasterki. Razem z listkami bazylii ułożyć na pizzy.");
        System.out.println("4. Skropić oliwą. (Szczególnie dobrze skropić bazylię).");
        System.out.println("5. Każdą blachę piec w nagrzanym piekarniku ok. 20 minut w temperaturze 220°C.");
    }

    @Override
    public List<String> getIngredients() {

        return ingredients;
    }
}

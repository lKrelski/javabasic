package com.sda.oop.example;

import com.sda.oop.example.engines.DieselEngine;
import com.sda.oop.example.engines.ElectricEngine;
import com.sda.oop.example.engines.PetrolEngine;
import com.sda.oop.example.engines.TurboPetrolEngine;
import com.sda.oop.example.vehicle.Car;
import com.sda.oop.example.vehicle.SportCar;
import com.sda.oop.example.vehicle.SuperSportCar;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Car car = new Car("niebieski", new PetrolEngine());
        car.startEngine();
        System.out.println(car.isEngineRunning());
        car.stopEngine();
        System.out.println(car.isEngineRunning());
        System.out.println(car);

        SportCar sportCar = new SportCar("niebieski", new ElectricEngine());
        Car sportCar1 = new SportCar("niebieski", new DieselEngine());

        car.startEngine();
        sportCar.startEngine();
        sportCar1.startEngine();
        System.out.println("**************************** Demonstracja dziedziczenia ***********************");
        Car samochod1 = new Car("czerwony", new DieselEngine());
        SportCar samochod2 = new SportCar("czarny", new ElectricEngine());
        SuperSportCar samochod3 = new SuperSportCar("niebieski", new PetrolEngine());
        SuperSportCar samochod4 = new SuperSportCar("czarny", new TurboPetrolEngine());
        List<Car> samochody = new ArrayList<>();
        samochody.add(samochod1);
        samochody.add(samochod2);
        samochody.add(samochod3);
        samochody.add(samochod4);
        for (Car car1 : samochody) {
            System.out.println("Uruchamianie samochodu");
            car1.startEngine();
        }
    }
}

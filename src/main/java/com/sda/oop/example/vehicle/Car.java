package com.sda.oop.example.vehicle;

import com.sda.oop.example.engines.Engine;
import lombok.Getter;
import lombok.ToString;

/**
 * This is base class for all cars
 */
@ToString
public class Car {
    @Getter
    private Engine engine;
    @Getter
    private boolean engineRunning;
    @Getter
    private String colour;
    @Getter
    protected int numberOfSeats = 5;

    public Car(String colour, Engine engine) {
        this.colour = colour;
        this.engine = engine;
    }

    /**
     * Start engine
     */
    public void startEngine() {
        System.out.println("Uruchamianie silnika samochodu");
        engine.startEngine();
        engineRunning = true;
    }

    /**
     * Stop engine
     */
    public void stopEngine() {
        System.out.println("Zatrzymywanie silnika samochodu");
        engineRunning = false;
    }

}

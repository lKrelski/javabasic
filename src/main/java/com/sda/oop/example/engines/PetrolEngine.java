package com.sda.oop.example.engines;


public class PetrolEngine extends Engine {
    @Override
    public void startEngine() {
        System.out.println("Uruchamianie silnika benzynowego");
    }

    @Override
    public int getOilLevel() {
        return 80;
    }

    @Override
    public int getCoolingFluidLevel() {
        return 80;
    }
}

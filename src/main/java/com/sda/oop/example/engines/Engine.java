package com.sda.oop.example.engines;

public abstract class Engine implements EngineInterface {
    private float capacity;

    public abstract void startEngine();

    public float getCapacity() {
        return capacity;
    }
}


package com.sda.oop.example.engines;

public class DieselEngine extends Engine {
    @Override
    public void startEngine() {
        System.out.println("Uruchamianie silnika diesla");
    }

    @Override
    public int getOilLevel() {
        return 90;
    }

    @Override
    public int getCoolingFluidLevel() {
        return 90;
    }
}

package com.sda.oop.example.engines;

public interface EngineInterface {
    int getOilLevel();

    int getCoolingFluidLevel();
}

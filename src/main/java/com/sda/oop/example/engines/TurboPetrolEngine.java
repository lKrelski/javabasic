package com.sda.oop.example.engines;

public class TurboPetrolEngine extends Engine {
    @Override
    public void startEngine() {
        System.out.println("Uruchamianie silnika turboPetrol");
    }

    @Override
    public int getOilLevel() {
        return 95;
    }

    @Override
    public int getCoolingFluidLevel() {
        return 95;
    }
}

package com.sda.regularexpression.zadanie11;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlateValidator {
    public boolean validate(String plate){
        Pattern pattern = Pattern.compile("^[A-Z]{2}[0-9]{5}$");
        Pattern pattern1 = Pattern.compile("^[A-Z]{2}[0-9]{4}[A-Z]{1}$");
        Pattern pattern2 = Pattern.compile("^[A-Z]{2}[0-9]{3}[A-Z]{2}$");
        Matcher matcher = pattern.matcher(plate);
        Matcher matcher1 = pattern1.matcher(plate);
        Matcher matcher2 = pattern2.matcher(plate);
        if (matcher.matches()||matcher1.matches()||matcher2.matches()){
            System.out.println("Poprawna tablica rejestracyjna.");
        }else {
            System.out.println("Błędna tablica rejestracyjna");
        }return true;
    }

}

package com.sda.regularexpression.zadanie11;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj tablicę rejestracyjną: ");
        String plate = scanner.nextLine();
        new PlateValidator().validate(plate);
    }
}

package com.sda.regularexpression.zadanie8;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TelephoneValidator {
    public boolean validate(String telephone) {
        Pattern pattern = Pattern.compile("^[0-9]{9}$");
        Pattern pattern1 = Pattern.compile("^[0-9]{3}-[0-9]{3}-[0-9]{3}");
        Pattern pattern2 = Pattern.compile("^[0-9]{3} [0-9]{3} [0-9]{3}");
        Pattern pattern3 = Pattern.compile("^\\+[0-9]{2}[0-9]{9}$");
        Pattern pattern4 = Pattern.compile("^\\+[0-9]{2}[0-9]{3}-[0-9]{3}-[0-9]{3}");
        Pattern pattern5 = Pattern.compile("^\\+[0-9]{2}[0-9]{3} [0-9]{3} [0-9]{3}");
        Pattern pattern6 = Pattern.compile("^\\+[0-9]{2}-[0-9]{3}-[0-9]{3}-[0-9]{3}");
        Pattern pattern7 = Pattern.compile("^\\+[0-9]{2} [0-9]{3} [0-9]{3} [0-9]{3}");
        Matcher matcher = pattern.matcher(telephone);
        Matcher matcher1 = pattern1.matcher(telephone);
        Matcher matcher2 = pattern2.matcher(telephone);
        Matcher matcher3 = pattern3.matcher(telephone);
        Matcher matcher4 = pattern4.matcher(telephone);
        Matcher matcher5 = pattern5.matcher(telephone);
        Matcher matcher6 = pattern6.matcher(telephone);
        Matcher matcher7 = pattern7.matcher(telephone);
        if (matcher.matches() || matcher3.matches()) {
            System.out.println("Numer poprawny.");
        } else if (matcher1.matches() || matcher4.matches() || matcher6.matches()) {
            System.out.println("Numer poprawny.");
        } else if (matcher2.matches() || matcher5.matches() || matcher7.matches()) {
            System.out.println("Numer poprawny.");
        } else {
            System.out.println("Numer niepoprawny");
        }
        return true;
    }
}
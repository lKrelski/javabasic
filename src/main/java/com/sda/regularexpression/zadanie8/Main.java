package com.sda.regularexpression.zadanie8;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Wprowadź numer telefonu: ");
        String numerTelefonu = scanner.nextLine();
        new TelephoneValidator().validate(numerTelefonu);

    }
}

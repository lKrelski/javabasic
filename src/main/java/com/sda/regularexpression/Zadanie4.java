package com.sda.regularexpression;

import com.sda.Utils;

import java.util.regex.Pattern;

public class Zadanie4 {
    public static void main(String[] args) {
        int liczba = Utils.pobierzLiczbeZKonsoli();
        boolean jestOk = Pattern.matches("^[0-9]{3}$", Integer.toString(liczba));
        if(jestOk){
            System.out.println("Liczba jest poprawna");
        }else {
            System.out.println("Liczba jest niepoprawna");
        }
    }
}

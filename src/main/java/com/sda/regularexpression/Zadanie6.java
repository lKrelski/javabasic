package com.sda.regularexpression;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Zadanie6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Wprowadź tekst: ");
        String znaki = scanner.nextLine();
        boolean jestOk = Pattern.matches("^[a-zA-Z0-9]+$", znaki);
        if (jestOk){
            System.out.println("Ciąg znaków zgodny.");
        }else {
            System.out.println("Ciąg znaków błędny.");
        }
    }
}

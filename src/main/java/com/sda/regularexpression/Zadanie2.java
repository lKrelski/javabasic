package com.sda.regularexpression;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Zadanie2 {
    public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Wprowadz tekst: ");
            String tekst = scanner.nextLine();

            Pattern pattern = Pattern.compile("^[a-z]+$");
            Matcher matcher = pattern.matcher(tekst);
            if (matcher.matches()){
                System.out.println("Wprowadzony tekst zawiera tylko małe litery");
            }else {
                System.out.println("Wprowadzony tekst jest niepoprawny");
            }
        }
    }


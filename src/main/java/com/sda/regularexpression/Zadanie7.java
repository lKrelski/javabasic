package com.sda.regularexpression;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Zadanie7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Wprowadź kod pocztowy: ");
        String kodPocztowy = scanner.nextLine();
        Pattern pattern = Pattern.compile("^[0-9]{2}-[0-9]{3}$");
        Matcher matcher = pattern.matcher(kodPocztowy);
        if (matcher.matches()){
            System.out.println("Kod pocztowy poprawy.");
        }else {
            System.out.println("Kod pocztowy błędy.");
        }
    }
}

package com.sda.regularexpression;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Zadanie3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Wprowadź znaki: ");
        String tekst = scanner.nextLine();
        Pattern pattern = Pattern.compile("^[0-9]+$");
        Matcher matcher = pattern.matcher(tekst);
        if (matcher.matches()){
            System.out.println("Wprowadzono tylko liczby.");
        }else {
            System.out.println("Wprowadzony ciąg znaków jest niepoprawny.");
        }
    }
}

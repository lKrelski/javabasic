package com.sda.regularexpression.zadanie10;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Wprowadź imię: ");
        String imie = scanner.nextLine();
        new NameValidator().validate(imie);
    }
}

package com.sda.rozne.zadanie3;

import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj liczbę z przedziału [1-99] : ");
        Integer number = scanner.nextInt();
        HashMap<Integer, String> mapa = new HashMap<>();
        mapa.put(1, "jeden");
        mapa.put(2, "dwa");
        mapa.put(3, "trzy");
        mapa.put(4, "cztery");
        mapa.put(5, "pięć");
        mapa.put(6, "sześć");
        mapa.put(7, "siedem");
        mapa.put(8, "osiem");
        mapa.put(9, "dziewięć");
        mapa.put(10, "dziesięć");
        mapa.put(11, "jedenaaście");
        mapa.put(12, "dwanaście");
        mapa.put(13, "trzynaście");
        mapa.put(14, "czternaście");
        mapa.put(15, "pietnaście");
        mapa.put(16, "szesnaście");
        mapa.put(17, "siedemnaście");
        mapa.put(18, "osiemnaście");
        mapa.put(19, "dziewietnaście");
        mapa.put(20, "dwadzieścia");
        mapa.put(30, "trzydzieści");
        mapa.put(40, "czterdzieści");
        mapa.put(50, "pięćdziesiąt");
        mapa.put(60, "sześćdziesiąt");
        mapa.put(70, "siedemdziesiąt");
        mapa.put(80, "osiemdziesiąt");
        mapa.put(90, "dziewięćdziesiąt");
        if (number < 100 && number!=0) {
            if (mapa.containsKey(number)) {
                String wartosc = mapa.get(number);
                System.out.println(wartosc);
            } else if (number > 20) {
                String klucz = number.toString();
                klucz.toCharArray();
                if (klucz.charAt(1) > 0) {
                    String klucz1 = mapa.get(Character.getNumericValue(klucz.charAt(0)) * 10);
                    String klucz2 = mapa.get(Character.getNumericValue(klucz.charAt(1)));
                    System.out.println(klucz1 + " " + klucz2);
                }
                }
            }else {
            System.out.println("Liczba poza przedziałem. ");
        }


    }

}
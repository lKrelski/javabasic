package com.sda.rozne.zadanie5;

import java.util.Scanner;

import static com.sda.rozne.zadanie5.IsPalidrome.isPalindrome;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Wprowadź tekst: ");
        String tekst = scanner.nextLine();
        String tekstBezSpacji = tekst.replaceAll("\\s+", "").toLowerCase();
        boolean ok = isPalindrome(tekstBezSpacji);
        if (ok == true) {
            System.out.println("Wprowadzony ciąg znaków " + "\"" + tekst + "\"" + " jest palidromem");
        } else {
            System.out.println("Wprowadzony ciąg znaków " +"\""+ tekst +"\""+ " nie jest palidromem");
        }
    }

}


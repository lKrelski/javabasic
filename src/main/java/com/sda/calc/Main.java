package com.sda.calc;

import com.sda.arrays.Zadanie10;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program wykonuje wskazaną operację dla 2 podanych liczb");
        System.out.print("Podaj pierwszą liczbę: ");
        int liczba1 = scanner.nextInt();
        System.out.print("Podaj drugą liczbę: ");
        int liczba2 = scanner.nextInt();
        System.out.print("Podaj operację (+-*/): ");
        scanner.nextLine();
        String operacja = scanner.nextLine();


        Float wynikOperacji = null;
        if (liczba1 >= 0 && liczba2 >= 0) {
            switch ( operacja.charAt(0) ) {
                case '*':
                    wynikOperacji = (float) liczba1 * liczba2;
                    break;
                case '+':
                    wynikOperacji = (float) liczba1 + liczba2;
                    break;
                case '-':
                    wynikOperacji = (float) liczba1 - liczba2;
                    break;
                case '/':
                    if (liczba2 == 0) {
                        System.out.println("Nie można dzielić przez 0!");
                    } else {
                        wynikOperacji = (float) liczba1 / liczba2;
                    }
                    break;
                default:
                    System.out.println("Nie rozpoznaje operacji.");
            }
            if (wynikOperacji != null) {
                System.out.print("Wynik wynosi: " + wynikOperacji);
            }
            } else {
                System.out.println("Podałeś liczby umjemne. Nie umiem sumować takich liczb");
            }
        }
    }

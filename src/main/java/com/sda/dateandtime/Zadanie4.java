package com.sda.dateandtime;

import java.time.LocalDate;
import java.time.Period;

public class Zadanie4 {
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.of(2017,01,01);
        LocalDate localDate1 = LocalDate.of(2017,05,05);
        System.out.println(Period.between(localDate,localDate1));
    }
}

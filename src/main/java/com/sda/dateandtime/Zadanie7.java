package com.sda.dateandtime;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Zadanie7 {
    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy 'roku' EEEE HH:mm:ss");
        String tekst = localDateTime.format(formatter);
        System.out.println(tekst);
    }
}

package com.sda.dateandtime;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class Zadanie6 {
    public static void main(String[] args) {
        ZoneId tokyo = ZoneId.of("Asia/Tokyo");
        LocalDateTime localDateTime = LocalDateTime.now(tokyo);
        System.out.println("Bieżąca data i czas w Tokio : " + localDateTime);
    }
}

package com.sda.dateandtime;

import java.time.Duration;
import java.time.LocalTime;

public class Zadanie5 {
    public static void main(String[] args) {
        LocalTime localTime = LocalTime.of(14,11);
        LocalTime localTime1 = LocalTime.of(18,41);
        System.out.println(Duration.between(localTime,localTime1));
    }
}

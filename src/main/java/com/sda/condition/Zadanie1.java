package com.sda.condition;

import com.sda.Utils;

public class Zadanie1 {
    public static void main(String[] args) {
        int liczbaZKonsoli = Utils.pobierzLiczbeZKonsoli();
        if (liczbaZKonsoli > 0) {
            System.out.println("Wprowadzona liczba jest dodatnia");
        } else {
            System.out.println("Wprowadzona liczba jest ujemna");
        }
    }
}

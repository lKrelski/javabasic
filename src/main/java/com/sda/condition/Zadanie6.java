package com.sda.condition;

import com.sda.Utils;

public class Zadanie6 {
    public static void main(String[] args) {
        int liczba = Utils.pobierzLiczbeZKonsoli();
        if (liczba > 0) {
            if (liczba % 2 == 0) {
                System.out.println("Liczba jest parzysta");
            } else {
                System.out.println("Liczba jest nieparzysta");
            }
        } else System.out.println("Wprowadzona liczba jest ujemna - nie potrafię obliczyć");
    }
}

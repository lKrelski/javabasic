package com.sda.condition;

import com.sda.Utils;

import java.util.Scanner;

public class Zadanie9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj liczbę: ");
        int liczba = scanner.nextInt();
        if (liczba <= 10)
            switch ( liczba ) {
                case 1:
                    System.out.println("Wprowadzono liczbę jeden.");
                    break;
                case 2:
                    System.out.println("Wprowadzono liczbę dwa.");
                    break;
                case 3:
                    System.out.println("Wprowadzono liczbę trzy.");
                    break;
                case 4:
                    System.out.println("Wprowadzono liczbę cztery.");
                    break;
                case 5:
                    System.out.println("Wprowadzono liczbę pięć.");
                    break;
                case 6:
                    System.out.println("Wprowadzono liczbę sześć.");
                    break;
                case 7:
                    System.out.println("Wprowadzono liczbę siedem.");
                    break;
                case 8:
                    System.out.println("Wprowadzono liczbę osiem.");
                    break;
                case 9:
                    System.out.println("Wprowadzono liczbę dziewięć.");
                    break;
                case 10:
                    System.out.println("Wprowadzono liczbę dziesięć");
                    break;

            }
        else {
            System.out.println("Liczba " + liczba + " jest spoza dozwolonego zakresu <1,10>");
        }
    }
}
package com.sda.condition;

import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Wprowadź pierwszą liczbę: ");
        float liczba1 = scanner.nextFloat();
        System.out.print("Wprowadź drugą liczbę: ");
        float liczba2 = scanner.nextFloat();
        if (-0.001 <= liczba1 - liczba2 && liczba1 - liczba2 <= 0.001) {
            System.out.println("Liczby: " + liczba1 + " i " + liczba2 + " są takie same.");
        } else {
            System.out.println("Liczby: " + liczba1 + " i " + liczba2 + " są różne.");
        }
    }
}

package com.sda.condition;

import com.sda.Utils;

public class Zadanie2 {
    public static void main(String[] args) {
        int liczba1 = Utils.pobierzLiczbeZKonsoli();
        int liczba2 = Utils.pobierzLiczbeZKonsoli();
        if (liczba1 > liczba2) {
            System.out.println("Liczba " + liczba1 + " jest większa od liczby " + liczba2);
        } else if (liczba1 < liczba2) {
            System.out.println("Liczba " + liczba2 + " jest większa od liczby " + liczba1);
        } else {
            System.out.println("Liczba " + liczba1 + " jest równa liczbie " + liczba2);
        }
    }
}

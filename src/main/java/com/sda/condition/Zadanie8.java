package com.sda.condition;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Zadanie8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj imię: ");
        String imie = scanner.nextLine();
        Pattern pattern = Pattern.compile("^[A-Z]{1}[a-z]+$");
        Pattern pattern1 = Pattern.compile("^[A-Z]{1}[a-z]+[a]$");
        Matcher matcher = pattern.matcher(imie);
        Matcher matcher1 = pattern1.matcher(imie);
        if (matcher.matches()) {
            if (matcher1.matches()) {
                System.out.println("Wprowadziłeś poprawne imię. Witaj " + imie + ". Jesteś kobietą.");
            } else {
                System.out.println("Wprowadziłeś poprawne imię. Witaj " + imie + ". Jesteś mężczyzną.");
            }
        } else {
            System.out.println("Wprowadzone imię zawiera błąd ortograficzny. Imiona piszemy wielką literą");
        }
    }
}

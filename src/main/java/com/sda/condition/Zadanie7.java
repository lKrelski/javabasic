package com.sda.condition;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Zadanie7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj imię: ");
        String imie = scanner.nextLine();
        Pattern pattern = Pattern.compile("^[A-Z]{1}.*$");
        Matcher matcher = pattern.matcher(imie);
        if (matcher.matches()) {
            System.out.println("Wprowadziłeś poprawne imię. Witaj " + imie);
        } else {
            System.out.println("Wprowadzone imię zawiera błąd ortograficzny. Imiona piszemy wielką literą");
        }
    }
}

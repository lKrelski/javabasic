package com.sda.condition;

import java.util.Scanner;

public class Zadanie5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Wprowadź tekst: ");
        String tekst = scanner.nextLine();
        char[] znaki = tekst.toCharArray();
        if (znaki.length < 10) {
            System.out.println("Wprowadzony tekst jest za krótki.");
        } else {
            System.out.println("Wprowadzony tekst: " + "\"" + tekst + "\"" + " spełnia kryteria.");
        }
    }
}

package com.sda.condition;

import com.sda.Utils;

public class Zadanie3 {
    public static void main(String[] args) {
        int liczba1 = Utils.pobierzLiczbeZKonsoli();
        int liczba2 = Utils.pobierzLiczbeZKonsoli();
        int liczba3 = Utils.pobierzLiczbeZKonsoli();
        int najwiekszaLiczba;
        int najmniejszaLiczba;
        najwiekszaLiczba = wyszukajNajwiekszaLiczba(liczba1, liczba2, liczba3);
        najmniejszaLiczba = wyszukajNajmniejszaLiczba(liczba1, liczba2, liczba3);

        System.out.println("Największa liczba to: " + najwiekszaLiczba);
        System.out.println("Najmniejsza liczba to: " + najmniejszaLiczba);
    }

    private static int wyszukajNajwiekszaLiczba(int liczba1, int liczba2, int liczba3) {
        int najwiekszaLiczba;
        if (liczba1 > liczba2) {
            if (liczba1 > liczba3) {
                najwiekszaLiczba = liczba1;
            } else {
                najwiekszaLiczba = liczba3;
            }
        } else {
            if (liczba2 > liczba3) {
                najwiekszaLiczba = liczba2;
            } else {
                najwiekszaLiczba = liczba3;
            }
        }
        return najwiekszaLiczba;
    }

    private static int wyszukajNajmniejszaLiczba(int liczba1, int liczba2, int liczba3) {
        int najmniejszaLiczba;
        if (liczba1 < liczba2) {
            if (liczba1 < liczba3) {
                najmniejszaLiczba = liczba1;
            } else {
                najmniejszaLiczba = liczba3;
            }
        } else {
            if (liczba2 < liczba3) {
                najmniejszaLiczba = liczba2;
            } else {
                najmniejszaLiczba = liczba3;
            }
        }
        return najmniejszaLiczba;
    }
}
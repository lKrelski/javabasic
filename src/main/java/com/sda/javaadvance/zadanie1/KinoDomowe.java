package com.sda.javaadvance.zadanie1;

public class KinoDomowe {
    private Tv tv;
    private Glosniki glosniki;

    public KinoDomowe(String mojTV, int mocGlosnikow) {
        tv = new Tv(mojTV, 1);
        glosniki = new Glosniki(mocGlosnikow);
    }

    public void wlaczTV() {
        tv.on();
    }

    public void wylaczTV() {
        tv.off();
    }
    public void ustawGlosnosc(int glosnosc){
        glosniki.setMoc(glosnosc);
    }
    public void pokazGlosnosc(){
        glosniki.getMoc();
    }
}

package com.sda.javaadvance.zadanie1;

public class Device {
    public String nazwa;

    public Device(String nazwa) {
        this.nazwa = nazwa;
    }

    public void on(){
        System.out.println("Urządzenie " + nazwa+ " jest włączone.");
    }
    public void off(){
        System.out.println("Urządzenie "+ nazwa+ " jest wyłączone. ");
    }
}

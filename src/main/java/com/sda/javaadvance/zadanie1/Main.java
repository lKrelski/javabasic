package com.sda.javaadvance.zadanie1;

public class Main {
    public static void main(String[] args) {
        KinoDomowe kinoDomowe = new KinoDomowe("Samsung", 150);
        kinoDomowe.wlaczTV();
        kinoDomowe.ustawGlosnosc(500);
        kinoDomowe.pokazGlosnosc();
        kinoDomowe.wylaczTV();
    }
}

package com.sda.javaadvance.zadanie1;

public class Tv extends Device {
    int kanal;

    public Tv(String nazwa, int kanal) {
        super(nazwa);
        this.kanal = kanal;
    }

    @Override
    public void on() {
        System.out.println("Urządzenie " + nazwa + " jest włączone na kanale " + kanal);
    }

    @Override
    public void off() {
        System.out.println("Urządzenie " + nazwa + " jest wyłączone");
    }

    public  void zmienKanal(int kanal) {
        this.kanal = kanal;
    }

    public void wyswietlKanal() {
        System.out.println("Teraz TV " + nazwa + " jest włączone na kanale " + kanal);
    }
}

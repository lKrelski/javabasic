package com.sda.javaadvance.zadanie1;

public class Glosniki {
    int moc;

    public Glosniki(int moc) {
        this.moc = moc;
    }

    public void getMoc() {
        System.out.println("Głośność jest ustawiona na "+moc);
    }

    public void setMoc(int moc) {
        this.moc = moc;
    }
}

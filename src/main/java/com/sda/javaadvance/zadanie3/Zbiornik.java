package com.sda.javaadvance.zadanie3;

public class Zbiornik {
    private float iloscPaliwa;

    public float getIloscPaliwa() {
        return iloscPaliwa;
    }

    public void setIloscPaliwa(float iloscPaliwa) {
        this.iloscPaliwa = iloscPaliwa;
    }
}

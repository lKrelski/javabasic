package com.sda.javaadvance.zadanie2;


import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        DanePersonalne danePersonalne = new DanePersonalne(new OsobaReal("Adam", "Kowalski",
                LocalDate.of(1992,12,21)),new Adres("Długa","Toruń",1));
        System.out.println(danePersonalne);
    }
}

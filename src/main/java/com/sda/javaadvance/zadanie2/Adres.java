package com.sda.javaadvance.zadanie2;

public class Adres {
    private String ulica;
    private String miasto;
    private int nrDomu;

    public Adres(String ulica, String miasto, int nrDomu) {
        this.ulica = ulica;
        this.miasto = miasto;
        this.nrDomu = nrDomu;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public String getMiasto() {
        return miasto;
    }

    public void setMiasto(String miasto) {
        this.miasto = miasto;
    }

    public int getNrDomu() {
        return nrDomu;
    }

    public void setNrDomu(int nrDomu) {
        this.nrDomu = nrDomu;
    }

    @Override
    public String toString() {
        return "Adres{" +
                "ulica='" + ulica + '\'' +
                ", miasto='" + miasto + '\'' +
                ", nrDomu=" + nrDomu +
                '}';
    }
}

package com.sda.javaadvance.zadanie2;

import java.time.LocalDate;

public class OsobaReal implements Osoba {
    private String imie;
    private String nazwisko;
    private LocalDate dataUrodzenia;


    public OsobaReal(String imie, String nazwisko, LocalDate dataUrodzenia) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;

    }

    @Override
    public String getImie() {
        return imie;
    }

    @Override
    public void setImie(String imie) {

    }

    @Override
    public String getNazwisko() {
        return nazwisko;
    }

    @Override
    public void setNazwisko(String nazwisko) {

    }

    @Override
    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    @Override
    public void getDataUrodzenia(LocalDate dataUrodzenia) {

    }

    @Override
    public String toString() {
        return "OsobaReal{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", dataUrodzenia=" + dataUrodzenia +
                '}';
    }
}
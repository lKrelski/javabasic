package com.sda.javaadvance.zadanie2;

import com.sda.oop.zadanie2.Address;

public class DanePersonalne {
    private OsobaReal osoba;
    private Adres adres;

    public DanePersonalne(OsobaReal osoba, Adres adres) {
        this.osoba = osoba;
        this.adres = adres;
    }

    public OsobaReal getOsoba() {
        return osoba;
    }

    public void setOsoba(OsobaReal osoba) {
        this.osoba = osoba;
    }

    public Adres getAdres() {
        return adres;
    }

    public void setAdres(Adres adres) {
        this.adres = adres;
    }

    @Override
    public String toString() {
        return "DanePersonalne{" +
                "osoba=" + osoba +
                ", adres=" + adres +
                '}';
    }
}

package com.sda.javaadvance.zadanie2;

import java.time.LocalDate;


public interface Osoba {
    String getImie();
    void setImie(String imie);
    String getNazwisko();
    void setNazwisko(String nazwisko);
    LocalDate getDataUrodzenia();
    void getDataUrodzenia(LocalDate dataUrodzenia);
}

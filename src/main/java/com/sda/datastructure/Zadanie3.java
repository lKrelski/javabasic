package com.sda.datastructure;

import java.util.LinkedList;
import java.util.Queue;

public class Zadanie3 {
    public static void main(String[] args) {
        Queue<Person> kolejkaOsob = new LinkedList<>();
        kolejkaOsob.add(new Person("John", "Kowalski"));
        kolejkaOsob.add(new Person("Bob", "Nowak"));
        kolejkaOsob.add(new Person("Andrzej", "Gruby"));
        kolejkaOsob.add(new Person("Kamil", "Kolanko"));
        kolejkaOsob.add(new Person("Paweł", "Chudy"));
        kolejkaOsob.add(new Person("Krzysztof", "Dzban"));
        kolejkaOsob.add(new Person("Zbigniew", "Kalski"));
        kolejkaOsob.add(new Person("Borys", "Dzbanowski"));
        kolejkaOsob.add(new Person("Grzegorz", "Debil"));
        kolejkaOsob.add(new Person("Adam", "Retard"));
        Person pierwszaOsoba = kolejkaOsob.peek();
        Person ostatniaOsoba = null;
        int iloscOsobWKolejce = kolejkaOsob.size();
        for (int i = 0; i < iloscOsobWKolejce; i++) {
            ostatniaOsoba = kolejkaOsob.poll();
        }
        System.out.println("Pierwsza osoba: " + pierwszaOsoba.getName() + " "
                + pierwszaOsoba.getLastName());
        if (ostatniaOsoba != null) {
            System.out.println("Ostatnia osoba: " + ostatniaOsoba.getName() + " "
                    + ostatniaOsoba.getLastName());
        }
    }
}
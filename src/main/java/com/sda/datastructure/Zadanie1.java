package com.sda.datastructure;

import java.util.TreeSet;

public class Zadanie1 {
    public static void main(String[] args) {
        TreeSet<String> listaImion = new TreeSet<>();
        listaImion.add("Lukasz");
        listaImion.add("John");
        listaImion.add("Bob");
        listaImion.add("Ania");
        listaImion.add("Krystian");
        for (String imie : listaImion) {
            System.out.println(imie);
        }
    }
}


package com.sda.datastructure;

import java.util.LinkedList;
import java.util.Queue;

public class Zadanie5 {
    public static void main(String[] args) {
        Queue<String> zadania = new LinkedList<>();
        ((LinkedList<String>) zadania).add(0, "Umyj okna");
        ((LinkedList<String>) zadania).add(1, "Odkurz");
        ((LinkedList<String>) zadania).add(2, "Umyj podłogi");
        ((LinkedList<String>) zadania).add(3, "Zrób pranie");
        ((LinkedList<String>) zadania).add(4, "Zmień pościel");
        ((LinkedList<String>) zadania).add(5, "Zadzwoń do mamy");
        ((LinkedList<String>) zadania).add(6, "Zrób zakupy");
        ((LinkedList<String>) zadania).add(7, "Najeb się");
        ((LinkedList<String>) zadania).add(8, "Przeproś Anię");
        ((LinkedList<String>) zadania).add(9, "Umyj się");
        int i = 1;
            for (String listaZadan : zadania) {
                System.out.println((i++) + ". " + listaZadan);
            }
        }
    }

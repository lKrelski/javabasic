package com.sda.datastructure;

import java.util.HashSet;

public class Zadanie4 {
    public static void main(String[] args) {
        HashSet<String> matryce = new HashSet<>();
        matryce.add("Bob");
        matryce.add("Noob");
        matryce.add("Bożena");
        matryce.add("Sebiks");
        matryce.add("Karyna");
        matryce.add("Janusz");
        matryce.add("Grażyna");
        matryce.add("Andrzej");
        matryce.add("Lukasz");
        matryce.add("John");
        matryce.add("John");
        matryce.add("John");
        matryce.add("John");
        for (String kolekcja : matryce) {
            System.out.println(kolekcja);
        }
    }
}

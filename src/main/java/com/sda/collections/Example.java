package com.sda.collections;

import java.util.*;

public class Example {
    public static void main(String[] args) {
        //Tworzenie listy
        List<String> listaString = new ArrayList<String>();
        //dodawanie elementów
        listaString.add("Łukasz");
        listaString.add("Java");
        //pobieranie elementów
        listaString.get(0);//zwróci Łukasz
        listaString.get(1);// zwróci Java
        //listaString.get(2);// zwróci IndexOutOfBoundsException
        listaString.add(0, "C#");//dodaje na zerowej pozycji
        listaString.get(0);//zwróci C#
        listaString.get(1);// zwróci Łukasz
        listaString.get(2);// zwróci Java
        //listaString.get(3);//zwróci IndexOutOfBoundsException
        int iloscElementowWLiscie = listaString.size();
        //Wyswietlenie wszystkich elementów z listy
        for (int i = 0; i < listaString.size(); i++) {
            System.out.println(listaString.get(i));
        }
        //listaString.forEach(element -> System.out.println(element));
        //Pobranie ostatniego elementu z listy
        listaString.get(listaString.size() - 1);

        //Używanie HashSet
        HashSet<String> worekMikolaja = new HashSet<>();
        boolean czyDodano = worekMikolaja.add("Java");//czyDodano ma watrość true
        czyDodano = worekMikolaja.add("Java");//czyDodano ma wartość false
        //Usuniecie wszystkich elementów
        worekMikolaja.clear();
        //ilosc elementów
        worekMikolaja.size();
        //usuniecie elementu
        worekMikolaja.remove("Java");
        for (String elementWKolekcji : worekMikolaja) {
            System.out.println(elementWKolekcji);
        }
        //Używanie HashMap
        HashMap<Integer, String> mapaTekstowa = new HashMap<>();
        mapaTekstowa.put(5, "pięć");//dodanie elementu do HashMap
        mapaTekstowa.put(11, "jedenascie");
        mapaTekstowa.put(1, "jeden");
        String wartosc = mapaTekstowa.get(5);//pobranie elementu
        System.out.println(wartosc);//na konsoli pojawi sie napis "pięć"
        int iloscElementow = mapaTekstowa.size();
        for (Integer klucz : mapaTekstowa.keySet()) {//wyswietlanie wszystkich kluczy w mapie
            System.out.print(klucz + " ");
        }
        for (Map.Entry<Integer, String> elementMapy : mapaTekstowa.entrySet()) {
            System.out.println("Klucz: " + elementMapy.getKey() + " Wartość: " + elementMapy.getValue());
        }
        boolean czyIstniejeKlucz = mapaTekstowa.containsKey(5);//true

        boolean czyIsteniejeWartosc = mapaTekstowa.containsValue("pięć");//true

        //Korzystanie z kolejki
        PriorityQueue<String> kolejka = new PriorityQueue<>();
        //Dodanie elementu do kolejki
        kolejka.add("Java");
        kolejka.add("MZK");
        kolejka.add("Cos");
        //Pobranie elementu z kolejki bez usuniecia go
        String elementZKolejki1 = kolejka.peek();
        //pobranie elementu z kolejki
        String elementZKolejki2 = kolejka.poll();
        //sprawdzenie ilości elementów w kolejce
        int IloscElementowWKolejce = kolejka.size();
    }
}

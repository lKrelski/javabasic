package com.sda.loops;

public class Zadanie3 {
    public static void main(String[] args) {
        int[] array = new int[] {4, 1, 123, 423, 5435, 3453, 123, 12342, 1231, 421};
        for (int i = 0; i <array.length ; i++) {
            String msg = "array[%s] = %s";
            System.out.println(String.format(msg, i, array[i]));

            //System.out.println("array["+i+ "] = "+array[i]);

        }
    }
}

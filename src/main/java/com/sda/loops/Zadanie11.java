package com.sda.loops;

import com.sda.Utils;

public class Zadanie11 {
    public static void main(String[] args) {
        int liczik = 0;
        int ileLiczb = Utils.pobierzLiczbeZKonsoli();
        do {
            liczik++;
            System.out.print(liczik+" ");
        }while (liczik<ileLiczb);
    }
}

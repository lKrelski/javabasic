package com.sda.loops;

public class Zadanie12 {
    public static void main(String[] args) {
        int[] array = new int[] {4, 1, 123, 423, 5435, 3453, 123, 12342, 1231, 421,1432,45,3432,123,4,1};
        int i = -1;
        do {
            i++;
            String msg = "array[%s] = %s";
            System.out.println(String.format(msg,i,array[i]));
        }while (i<array.length-1);
    }
}

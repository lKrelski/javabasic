package com.sda.loops;

public class Zadanie9 {
    public static void main(String[] args) {
        int[] array = new int[] {4, 1, 123, 423, 5435, 3453, 123, 12342, 1231, 421};
        int i = -1;
        while (i<array.length-1){
            i++;
            String msg = "array[%s] = %s";
            System.out.println(String.format(msg,i,array[i]));
        }
    }
}

package com.sda.loops;

import com.sda.Utils;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> liczbyLosowe = new ArrayList<>();
        Random generator = new Random();
        System.out.print("Podaj ile liczb wylosować: ");
        int liczba = scanner.nextInt();
        System.out.print("Podaj min liczbę: ");
        int liczbaMin = scanner.nextInt();
        System.out.print("Podaj max liczbę: ");
        int liczbaMax = scanner.nextInt();
        for (int i = 0; i < liczba; i++) {
            liczbyLosowe.add(generator.nextInt(((liczbaMax-liczbaMin)+1))+liczbaMin);
        }
        for (int i = 0; i < liczbyLosowe.size(); i++) {
            System.out.println(liczbyLosowe.get(i));
        }
    }
}



package com.sda.arrays;

import com.sun.deploy.util.ArrayUtil;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Zadanie11 {
    public static void main(String[] args) {
        int[] array1 = new int[]{32, 25, 522, 214, 5325, 6436, 123123, 4124124, 4214124, 123};
        new Zadanie11().reverse(array1);
    }
    public void reverse(int[] array) {
        for (int i = 0, j = array.length - 1;
             i < j; i++, j--) {
            // swap the values at the i and j indices
            int temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        System.out.print("Reverse Array :");
        for (int val : array) {
            System.out.print(" " + val);
        }
    }
}


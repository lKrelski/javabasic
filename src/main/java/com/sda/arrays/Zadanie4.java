package com.sda.arrays;

import java.util.stream.IntStream;

public class Zadanie4 {
    public static void main(String[] args) {
        int[] tablica = new int[10];
        tablica[0] = 12;
        tablica[1] = 32;
        tablica[2] = 4;
        tablica[3] = 1;
        tablica[4] = 34;
        tablica[5] = 23;
        tablica[6] = 11;
        tablica[7] = 33;
        tablica[8] = 12;
        tablica[9] = 45;
        //int suma = IntStream.of(tablica).sum();
        int suma = tablica[0] + tablica[1] + tablica[2] + tablica[3] + tablica[4] + tablica[5] + tablica[6] + tablica[7] + tablica[8] + tablica[9];
        float srednia = (float) suma / tablica.length;
        System.out.println(srednia);
    }
}

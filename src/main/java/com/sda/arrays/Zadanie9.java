package com.sda.arrays;

import java.util.Arrays;

public class Zadanie9 {
    public static void main(String[] args) {

        int[] array1 = new int[]{32, 25, 522, 214, 5325, 6436, 123123, 4124124, 4214124, 123};
        int[] array2 = new int[]{32, 25, 522, 214, 5325, 6436, 123123, 4124124, 4214124, 123};
        boolean sprawdzenie = new Zadanie9().equals(array1, array2);
        if (sprawdzenie == true) {
            System.out.println("Tablice są takie same");
        } else {
            System.out.println("Tablice są różne");
        }
    }

    public boolean equals(int[] array1, int[] array2) {
        if (Arrays.equals(array1, array2)) {
            return true;
        } else {
            return false;
        }
    }
}






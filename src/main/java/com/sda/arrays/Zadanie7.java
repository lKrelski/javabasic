package com.sda.arrays;

public class Zadanie7 {
    public static void main(String[] args) {
        int[] tablica = new int[]{11, 23, 45, 61, 2321};
        String tablicaWPostaciTekstowej = new Zadanie7().printArray(tablica);
        System.out.println(tablicaWPostaciTekstowej);
        //System.out.print(new Zadanie7().printArray(tablica));
    }

    public String printArray(int[] array) {
        String rezultat = "{ ";
        //Pętla wykona się 5 razy dla i=0,1,2,3,4. Liczba 5 nie spełnia warunku
        //bo array.lenght ma wartość 5
        for (int i = 0; i < array.length; i++) {
            rezultat += array[i];
            if (i != array.length - 1) {
                rezultat += ", ";
            }
        }
        rezultat += " }";
        return rezultat;
    }
}
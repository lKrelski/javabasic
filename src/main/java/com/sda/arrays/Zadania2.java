package com.sda.arrays;

import java.util.Arrays;

public class Zadania2 {
    public static void main(String[] args) {
        float[] tablica = new float[5];
        tablica[0] = 2.4f;
        tablica[1] = 32.7f;
        tablica[2] = 412.234f;
        tablica[3] = 4;
        tablica[4] = 5614;
        String tablicaWPostaciTekstowej = Arrays.toString(tablica);
        System.out.println(tablicaWPostaciTekstowej);
        //System.out.println(Arrays.toString(tablica));
    }
}

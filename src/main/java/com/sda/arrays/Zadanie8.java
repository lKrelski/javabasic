package com.sda.arrays;


import java.util.SortedMap;

public class Zadanie8 {
    public static void main(String[] args) {
        int[] array = new int[]{32, 25, 521, 214, 5325, 6436, 123123, 4124124, 4214124};
        boolean klucz = new Zadanie8().contains(array, 32);
        if (klucz == true){
            System.out.println("Znalazłem liczbę");
        }else {
            System.out.println("Nie znalazłem liczby");
        }

    }
    public boolean contains(int[] array, int key) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                return true;
            }
        }
        return false;
    }
}

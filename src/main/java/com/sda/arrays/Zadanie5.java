package com.sda.arrays;

public class Zadanie5 {
    public static void main(String[] args) {
        int[] tablica = new int[10];
        tablica[0] = 12;
        tablica[1] = 32;
        tablica[2] = 4;
        tablica[3] = 1;
        tablica[4] = 34;
        tablica[5] = 23;
        tablica[6] = 11;
        tablica[7] = 33;
        tablica[8] = 12;
        tablica[9] = 45;
        System.out.println(tablica.length);
    }
}

package com.sda.arrays;

import java.util.Arrays;

public class Zadanie10 {
    public static void main(String[] args) {
        int[] array1 = new int[]{32, 25, 522, 214, 5325, 6436, 123123, 4124124, 4214124, 123};
        int[] tablica = new Zadanie10().copyArray(array1);
        int[] cos = new int[]{123, 32423, 523412, 123124, 5325};
        int[] arr = new Zadanie10().copyArray(cos);
    }


    public int[] copyArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
            array = Arrays.copyOf(array, array.length);
        }
        return array;
    }
}
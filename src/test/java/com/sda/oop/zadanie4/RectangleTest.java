package com.sda.oop.zadanie4;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class RectangleTest {
    @Test
    public void shouldCountAreaCorectly() {
        Rectangle rectangle = new Rectangle(5, 4);
        Assert.assertTrue(rectangle.countArea() == 20f);
    }
}
